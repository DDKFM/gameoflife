package de.ddkfm.application;
	

import java.util.Optional;


import de.ddkfm.views.FieldNode;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;


public class Main extends Application {
	private Label lblHeading = new Label("Conways Spiel des Lebens");
	private Button btNextRound = new Button("N�chste Runde");
	private Button btIteration = new Button("Iterationen");
	private Button btReset = new Button("Reset");
	private RadioButton rbNeighborhood4 = new RadioButton("4-er Nachbarschaft");
	private RadioButton rbNeighborhood8 = new RadioButton("8-er Nachbarschaft");
	private ToggleGroup tgNeighborhood = new ToggleGroup();
	private RadioButton rbNormalRules = new RadioButton("Normale Regeln");
	private RadioButton rbCopyRules = new RadioButton("Kopierregeln");
	private ToggleGroup tgRules = new ToggleGroup();
	private FieldNode mainField = new FieldNode(500,500,25,25);
	@Override
	public void start(Stage primaryStage) {
		try {
			BorderPane root = new BorderPane();
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			
			lblHeading.getStyleClass().add("header");
			root.setTop(lblHeading);
			
			
			root.setCenter(mainField);
			
			rbNeighborhood4.setToggleGroup(tgNeighborhood);
			rbNeighborhood8.setToggleGroup(tgNeighborhood);
			rbNeighborhood8.setSelected(true);
			rbNormalRules.setToggleGroup(tgRules);
			rbCopyRules.setToggleGroup(tgRules);
			rbNormalRules.setSelected(true);
			
			root.setRight(new VBox(
							btReset,btNextRound,btIteration,
							new VBox(new Label("Nachbarschaft:"),rbNeighborhood4,rbNeighborhood8),
							new VBox(new Label("Regeln:"),rbNormalRules,rbCopyRules)
							)
						 );
			
			
			btNextRound.setOnAction(e->{
				mainField.nextRound();
			});
			btReset.setOnAction(e->{
				TextInputDialog dialog = new TextInputDialog("50");
				Optional<String> result = dialog.showAndWait();
				
				if(result.isPresent()){
					int size = 50;
					try {
						size = Integer.parseInt(result.get());
					} catch (NumberFormatException ex) {
						Alert al = new Alert(AlertType.ERROR);
						al.setContentText("Die von Ihnen eingegebene Zahl ist keine g�ltige Ganzzahl!\n"
								+ "Der Wert wird auf 50 gesetzt");
						al.show();
					}
					mainField = new FieldNode(500,500,size,size);
					rbNeighborhood8.setSelected(true);
					rbNormalRules.setSelected(true);
					root.setCenter(mainField);
				}
			});
			btIteration.setOnAction(e->{
				Timeline timer = new Timeline(
					new KeyFrame(Duration.millis(500),
						new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent event) {
								mainField.nextRound();
							}
						}));
				
				TextInputDialog dialog = new TextInputDialog("10");
				Optional<String> result = dialog.showAndWait();
				if(result.isPresent()){
					int count = 10;
					try {
						count = Integer.parseInt(result.get());
						timer.setCycleCount(count);
						timer.play();
					} catch (NumberFormatException ex) {
						dialog.close();
					}
				}
			});
			tgNeighborhood.selectedToggleProperty().addListener(new ChangeListener<Object>() {
				
				@Override
				public void changed(ObservableValue<? extends Object> arg0, Object arg1, Object arg2) {
					mainField.getLogic().setNeighborhood(rbNeighborhood8.isSelected()?8:4);
				}
			});
			tgRules.selectedToggleProperty().addListener(new ChangeListener<Object>() {
				
				@Override
				public void changed(ObservableValue<? extends Object> arg0, Object arg1, Object arg2) {
					mainField.getLogic().setNormalRules(rbNormalRules.isSelected());
				}
			});
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
