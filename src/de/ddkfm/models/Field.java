package de.ddkfm.models;

import java.util.Random;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class Field {
	private int fieldSizeX;
	private int fieldSizeY;
	private int neighborhood = 8;
	private boolean normalRules = true;
	
	private BooleanProperty[][] cells;
	
	private IntegerProperty round;
	
	public Field(int sizeX, int sizeY) {
		fieldSizeX = sizeX;
		fieldSizeY = sizeY;
		round = new SimpleIntegerProperty(0);
		cells = new SimpleBooleanProperty[sizeX][sizeY];
		for(int x = 0;x<sizeX;x++)
			for(int y = 0;y<sizeY;y++){
				cells[x][y] = new SimpleBooleanProperty(false);
			}
		generateStartStructure();
	}
	public void generateStartStructure(){
		generateFigur(fieldSizeX / 2 - 3, fieldSizeY / 2 - 3,new boolean[][]
					{
					 {true,true,true,true,true,true},
					 {true,false,true,true,false,true},
					 {true,true,true,true,true,true},
					 {true,true,true,true,true,true},
					 {true,false,true,true,false,true},
					 {true,true,true,true,true,true}
					}
					 );
	}
	private void generateFigur(int startX, int startY,boolean[][] figur){
		for(int x = 0; x < figur.length;x++)
			for(int y = 0;y < figur[x].length;y++){
				cells[startY + y][startX + x].set(figur[x][y]);
			}
	}
	public void nextRound(){
		round.set(round.get()+1);
		boolean[][] helpField = new boolean[fieldSizeX][fieldSizeY];
		for(int x = 0;x<fieldSizeX;x++)
			for(int y = 0;y<fieldSizeY;y++){
				helpField[x][y] = checkNeighbours(x,y);
			}
		for(int x = 0;x<fieldSizeX;x++)
			for(int y = 0;y<fieldSizeY;y++){
				cells[x][y].set(helpField[x][y]);
			}
	}
	private boolean checkNeighbours(int x , int y){
		boolean[] struct = new boolean[neighborhood];
		struct[0] = getCell(x,y-1);
		struct[1] = getCell(x-1,y);
		struct[2] = getCell(x+1,y);
		struct[3] = getCell(x,y+1);
		if(neighborhood == 8){
			struct[4] = getCell(x-1,y-1);
			struct[5] = getCell(x+1,y-1);
			struct[6] = getCell(x-1,y+1);
			struct[7] = getCell(x+1,y+1);
		}
		if(normalRules)
			if(getCell(x, y))
				if(getAliveCells(struct) < 2)
					return false;
				else
					if(getAliveCells(struct) == 2 || getAliveCells(struct) == 3)
						return true;
					else
						return false;
			else
				return getAliveCells(struct) == 3;
		else
			return getAliveCells(struct) % 2 == 1;
	}
	private int getAliveCells(boolean[] struct){
		int aliveCells = 0;
		for(boolean b : struct)
			if(b) aliveCells++;
		return aliveCells;
	}
	public boolean getCell(int x, int y){
		int newX, newY;
		newX = (x == -1)
				?fieldSizeX-1
				:((x==fieldSizeX)
					?0
					:x
				  );
		newY = (y == -1)
				?fieldSizeY-1
				:((y==fieldSizeY)
					?0
					:y
				  );
		return cells[newX][newY].get();
	}
	public void setCell(int x, int y,boolean value){
		cells[x][y].set(value);
	}
	public void setNormalRules(boolean value){
		normalRules = value;
	}
	public void setNeighborhood(int value){
		neighborhood = (value == 4 || value == 8)?value:8;
	}
	
	
}
