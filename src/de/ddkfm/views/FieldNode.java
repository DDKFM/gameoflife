package de.ddkfm.views;

import de.ddkfm.models.Field;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class FieldNode extends Pane{
	private double fitSizeX;
	private double fitSizeY;
	private Field logic;
	
	private Rectangle[][] cells;
	
	public FieldNode(double fitSizeX, double fitSizeY,int fieldX, int fieldY){
		this.fitSizeX = fitSizeX;
		this.fitSizeY = fitSizeY;
		logic = new Field(fieldX, fieldY);
		cells = new Rectangle[fieldX][fieldY];
		for(int x = 0;x<cells.length;x++)
			for(int y = 0;y<cells[x].length;y++){
				cells[x][y] = new Rectangle();
				cells[x][y].setWidth(fitSizeX / fieldX);
				cells[x][y].setHeight(fitSizeY / fieldY);
				cells[x][y].setLayoutX(x*fitSizeX / fieldX);
				cells[x][y].setLayoutY(y*fitSizeY / fieldY);
				if(logic.getCell(x, y))
					cells[x][y].setFill(Color.WHITE);
				else
					cells[x][y].setFill(Color.BLACK);
				this.getChildren().add(cells[x][y]);
			}
		this.setOnMouseClicked(e->{
			int x = (int)(e.getX() * fieldX / fitSizeX);
			int y = (int)(e.getY() * fieldY / fitSizeY);
			logic.setCell(x,y,!logic.getCell(x, y));
			loadFields();
		});
	}
	public void nextRound(){
		logic.nextRound();
		loadFields();
	}
	private void loadFields(){
		for(int x = 0;x<cells.length;x++)
			for(int y = 0;y<cells[x].length;y++)
				if(logic.getCell(x, y))
					cells[x][y].setFill(Color.WHITE);
				else
					cells[x][y].setFill(Color.BLACK);
	}
	public Field getLogic(){
		return logic;
	}
}
